package ru.t1.semikolenov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.semikolenov.tm.configuration.ApplicationConfiguration;
import ru.t1.semikolenov.tm.exception.field.EmptyLoginException;
import ru.t1.semikolenov.tm.exception.field.EmptyPasswordException;
import ru.t1.semikolenov.tm.marker.UnitCategory;
import ru.t1.semikolenov.tm.service.UserService;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class UserServiceTest {

    @NotNull
    @Autowired
    private UserService userService;

    @Test
    public void createUser() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.createUser("", "test", null));
        Assert.assertThrows(EmptyPasswordException.class, () -> userService.createUser("test", "", null));
        userService.createUser("user_test", "user_test", null);
        Assert.assertNotNull(userService.findByLogin("user_test"));
    }

    @Test
    public void findByLogin() {
        userService.createUser("user_test", "user_test", null);
        Assert.assertThrows(EmptyLoginException.class, () -> userService.findByLogin(null));
        Assert.assertNotNull(userService.findByLogin("user_test"));
    }

}
