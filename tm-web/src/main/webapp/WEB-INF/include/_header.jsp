<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Task Manager</title>
    <style>
        td {
            padding: 5px;
        }

        th {
            color: black;
            font-weight: 700;
            text-align: left;
            background: white;
        }
    </style>
</head>
<body>
<table width="100%" height="100%" border="1" style="padding: 10px; border-collapse: collapse;">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">

            <b>Task Manager</b>

        </td>

        <td with="100%", align="right">

            <sec:authorize access="isAuthenticated()">

                <a href="/projects">Projects</a>
                <span>|</span>
                <a href="/tasks">Tasks</a>
                <span>|</span>
                User: <sec:authentication property="name"/>
                <span>|</span>
                <a href="/logout">Logout</a>

            </sec:authorize>

            <sec:authorize access="!isAuthenticated()">
                <a href="/login">Login</a>
            </sec:authorize>

        </td>

    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="padding: 10px;">
