package ru.t1.semikolenov.tm.exception.entity;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
