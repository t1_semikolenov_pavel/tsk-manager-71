package ru.t1.semikolenov.tm.exception.field;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}