package ru.t1.semikolenov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.semikolenov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.semikolenov.tm.api.endpoint.IUserEndpoint;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.dto.request.*;
import ru.t1.semikolenov.tm.dto.response.UserLoginResponse;
import ru.t1.semikolenov.tm.dto.response.UserProfileResponse;
import ru.t1.semikolenov.tm.dto.response.UserRegistryResponse;
import ru.t1.semikolenov.tm.dto.response.UserUpdateResponse;
import ru.t1.semikolenov.tm.marker.ISoapCategory;
import ru.t1.semikolenov.tm.dto.model.UserDTO;
import ru.t1.semikolenov.tm.service.PropertyService;

@Category(ISoapCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(host, port);

    @Nullable
    private String adminToken;

    @Nullable
    private UserDTO userBefore;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin"));
        adminToken = loginResponse.getToken();
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.registryUser(new UserRegistryRequest(null, null, null, null)));
        userEndpoint.registryUser(new UserRegistryRequest(adminToken, "test_login_one", "test_password_one", ""));
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test_login_one", "test_password_one"));
        Assert.assertNotNull(loginResponse.getToken());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, "test_login_one"));
    }

    @Test
    public void removeUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "test_one", "test_one", ""));
        userBefore = registryResponse.getUser();
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.removeUser(new UserRemoveRequest(null, userBefore.getLogin())));
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test_one", "test_one")));
    }

    @Test
    public void updateUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "test_one", "test_one", ""));
        userBefore = registryResponse.getUser();
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test_one", "test_one"));
        @Nullable String token = loginResponse.getToken();
        @NotNull final String firstName = "first_name";
        @NotNull final String lastName = "last_name";
        @NotNull final String middleName = "middle_name";
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.updateUser(
                        new UserUpdateRequest(null, firstName, lastName, middleName)));
        @NotNull UserUpdateResponse response =
                userEndpoint.updateUser(new UserUpdateRequest(token, firstName, lastName, middleName));
        @Nullable UserDTO user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

    @Test
    public void showProfileUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "test_one", "test_one", ""));
        userBefore = registryResponse.getUser();
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("test_one", "test_one"));
        @Nullable String token = loginResponse.getToken();
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.showProfileUser(new UserProfileRequest(null)));
        @NotNull UserProfileResponse response = userEndpoint.showProfileUser(new UserProfileRequest(token));
        @Nullable UserDTO user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals("test_one", user.getLogin());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

    @Test
    public void changePassword() {
        @NotNull String oldPassword = "test_password_one";
        @NotNull String newPassword = "test_password_new";
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "test_one", oldPassword, ""));
        userBefore = registryResponse.getUser();
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test_one", oldPassword));
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
        userEndpoint.changePassword(new UserChangePasswordRequest(token, newPassword));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test_one", oldPassword)));
        @NotNull final UserLoginResponse responseAfterChange = authEndpoint.login(
                new UserLoginRequest("test_one", newPassword));
        Assert.assertNotNull(responseAfterChange.getToken());
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

    @Test
    public void lockUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "test_one", "test_one", ""));
        userBefore = registryResponse.getUser();
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.lockUser(new UserLockRequest(null, userBefore.getLogin())));
        userEndpoint.lockUser(new UserLockRequest(adminToken, userBefore.getLogin()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test_one", "test_one")));
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

    @Test
    public void unlockUser() {
        UserRegistryResponse registryResponse = userEndpoint.registryUser(
                new UserRegistryRequest(adminToken, "test_one", "test_one", ""));
        userBefore = registryResponse.getUser();
        userEndpoint.lockUser(new UserLockRequest(adminToken, userBefore.getLogin()));
        Assert.assertThrows(Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test_one", "test_one")));
        Assert.assertThrows(Exception.class,
                () -> userEndpoint.unlockUser(new UserUnlockRequest(null, userBefore.getLogin())));
        userEndpoint.unlockUser(new UserUnlockRequest(adminToken, userBefore.getLogin()));
        @NotNull final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test_one", "test_one"));
        @Nullable String token = response.getToken();
        Assert.assertNotNull(token);
        userEndpoint.removeUser(new UserRemoveRequest(adminToken, userBefore.getLogin()));
    }

}
